# CITES Coding Contest 2016

## Problema

Esta carpeta contiene la solución al problema "Recordando a Dennis". A partir de la consigna era necesario responder:

  1. ¿Cómo se llamaba el perro de Dennis?
  2. ¿Cuál era su color favorito?
  3. ¿De qué equipo era hincha?

## Repuestas

Las respuestas a las preguntas son:

 * El perro se llama "fido".
 * Su color favorito es el blanco (la clave es "white", en inglés).
 * Su equipo preferido son los "knicks" (clave de la tercera entrada del archivo shadow).


## Metodología

En el caso de este problema se consideró que la mejor herramienta para resolverlo era apelar al software de recuperación de claves "John the Ripper" (de ahora en más JTR), el cual se encuentra disponible de forma libre bajo licencia GPL. Dado que las reglas permiten arribar a la solución por vías alternativas, entiendo que esto es válido según el reglamento.

Dado que el programa JTR requiere que la entrada esté dada en la forma de un archivo passwd, se procedió a sintetizar manualmente un fichero passwd sin claves que fuera consistente con el shadow dado, y a combinarlos utilizando la herramienta "unshadow" que viene con JTR.

Luego de esto se ejecutó JTR contra cada una de las tres claves.

El JTR posee una heurística muy efectiva (probabilística) para determinar el orden más conveniente en que se deben ensayar las posibles combinaciones de letras candidatas a ser la clave, gracias a lo cual se lograron extraer las tres claves en tan solo algunos segundos.

## Cómo ejecutar las Soluciones

Para ejecutar la solución se debe tener instalado el software John The Ripper. Para instalarlo en Ubuntu:

	# sudo apt-get install john

Una vez que este programa está instalado se puede ejecutar la solución propuesta corriendo el script runall.sh que se encuentra en los archivos de la solución:

	# ./runall.sh


## Repositorio Git de la solución

Todos los archivos de la solución pueden hallarse en

https://gitlab.com/glpuga/acodearrecordandoadennis.git






