#!/bin/sh -

echo
echo

# Elimino los hashes hallados previamente...
echo "Borrando cache de claves previamente encontradas..."
# Esto lo quito por si alguien lo ejecuta y le borra claves que no quería perder xD
# rm ~/.john/john.pot 

# A partir de un archivo passwd ficticio y el shadow dado,
# genero un archivo passwd con hashes incluidos, como requiere
# John The Ripper

echo "Generando passwd a partir de passwd+shadow..."
unshadow pre_unshadow/passwd pre_unshadow/shadow > passwd

#
# Ahora a masticar hashes...

# Los hashes son de tres tipos diferentes; como John no puede buscar
# más de un tipo diferente de hash por vez, ataco los usuarios de a uno.

echo
echo "Atacando usuario dennis1..."
john -users:dennis1 passwd

echo
echo "Atacando usuario dennis2..."
john -users:dennis2 passwd

echo
echo "Atacando usuario dennis3..."

john -users:dennis3 passwd

echo
echo "Los passwords encontrados son..."
echo
john --show passwd | grep nologin |  cut -f2 -d:
echo
echo "Buenas noches, muchas gracias."

echo
echo